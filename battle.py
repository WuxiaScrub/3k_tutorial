from settings import*
from sprites import*
import os

os.chdir(RESOURCES_DIR)

class Battle:
    def __init__(self):
        self.game_width, self.game_height = LANDSCAPE_WIDTH, LANDSCAPE_HEIGHT
        os.environ['SDL_VIDEO_WINDOW_POS'] = "%d,%d" % (50, 50)
        self.screen = pg.display.set_mode((self.game_width, self.game_height))
        pg.display.set_caption(TITLE)
        self.clock = pg.time.Clock()
        self.running = True
        self.playing = True
        self.fps = FPS
        pg.font.init()
        pg.mixer.init()

        self.player_country = "Shu"
        self.enemy_country = "Wei"

        self.all_sprites = pg.sprite.Group()
        self.player_unit_sprites = pg.sprite.Group()
        self.enemy_unit_sprites = pg.sprite.Group()


    def new(self):

        self.player_selected_slot = 0
        self.enemy_selected_slot = 0
        self.player_morale = 100
        self.enemy_morale = 100
        self.enemy_last_deployed_time = 0

        self.background = Static(0, 0, "battleground_sand.png")
        self.all_sprites.add(self.background)

        self.player_deployment_action_bar = Static(0, 0, "sprite_action_bar.png")
        self.all_sprites.add(self.player_deployment_action_bar)

        self.general_action_bar = Static(400, 0, "sprite_action_bar.png")
        self.all_sprites.add(self.general_action_bar)

        self.enemy_deployment_action_bar = Static(800, 0, "sprite_action_bar.png")
        self.all_sprites.add(self.enemy_deployment_action_bar)

        self.slot_indicator = Static(30, self.player_selected_slot * 60 + 160, "sprite_slot_indicator_1.png")
        self.all_sprites.add(self.slot_indicator)

        self.run()


    def events(self):
        for event in pg.event.get():
            # Key release
            if event.type == pg.KEYUP:
                if event.key == pg.K_SPACE:
                    self.deploy_unit("left", "Elite_Sword_Infantry", self.player_selected_slot)

                if event.key == pg.K_UP:
                    self.toggle_slot(-1)
                elif event.key == pg.K_DOWN:
                    self.toggle_slot(1)


    def toggle_slot(self, delta):
        self.player_selected_slot += delta
        if self.player_selected_slot >= 8:
            self.player_selected_slot = 0
        elif self.player_selected_slot < 0:
            self.player_selected_slot = 7

        self.slot_indicator.rect.x, self.slot_indicator.rect.y = 30, self.player_selected_slot * 60 + 160


    def update(self):
        now = pg.time.get_ticks()
        self.all_sprites.update()
        if self.player_morale <= 0:
            self.player_morale = 0
            self.show_game_over_screen()
        elif self.enemy_morale <= 0:
            self.enemy_morale = 0
            self.show_game_over_screen(won=True)

        if now - self.enemy_last_deployed_time >= 100:
            self.enemy_last_deployed_time = now
            self.enemy_selected_slot = randrange(8)
            self.deploy_unit("right", "Elite_Sword_Infantry", self.enemy_selected_slot)


    def show_game_over_screen(self, won=False):
        self.dim_screen = pg.Surface(self.screen.get_size()).convert_alpha()
        self.dim_screen.fill((0, 0, 0, 200))
        self.screen.blit(self.dim_screen, (0, 0))
        if won:
            self.draw_text("Victory!", 40, YELLOW, self.game_width // 2, self.game_height * 3 // 5)
        else:
            self.draw_text("Defeat!", 40, RED, self.game_width // 2, self.game_height * 3 // 5)
        pg.display.flip()
        self.listen_for_key()


    def listen_for_key(self):
        waiting = True
        while waiting:
            if self.running:
                self.clock.tick(self.fps)

            for event in pg.event.get():
                if event.type == pg.KEYUP:
                    if event.key == pg.K_q:
                        self.playing = False
                        self.running = False
                        waiting = False


    def draw_text(self, text, size, color, x, y, anchor="midtop"):
        font = pg.font.Font("freesansbold.ttf", size)
        text_surface = font.render(text, True, color)
        text_rect = text_surface.get_rect()
        if anchor == "center":
            text_rect.center = (x, y)
        else:
            text_rect.midtop = (x, y)
        self.screen.blit(text_surface, text_rect)


    def draw(self):
        if self.playing:
            self.screen.fill(BLACK)
            self.all_sprites.draw(self.screen)
            pg.display.flip()


    def run(self):
        while self.playing:
            self.clock.tick(self.fps)
            self.events()
            self.update()
            self.draw()


    def deploy_unit(self, side, unit_type, slot=1):

        if side == "left":
            unit = Elite_Sword_Infantry(self, 50, slot*60 + 180, "Melee", unit_type, self.player_country, side, slot)
            self.player_unit_sprites.add(unit)
        else:
            unit = Elite_Sword_Infantry(self, self.game_width - 50, slot*60 + 180, "Melee", unit_type,
                                  self.enemy_country, side, slot)
            self.enemy_unit_sprites.add(unit)

        self.all_sprites.add(unit)


b = Battle()
b.new()